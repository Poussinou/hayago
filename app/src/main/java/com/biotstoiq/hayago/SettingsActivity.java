package com.biotstoiq.hayago;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.method.LinkMovementMethod;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

public class SettingsActivity extends Activity {

    static int hh, mm, ss;

    String[] stringTimeControls = {"Sudden Death", "Byo-yomi", "Canadian Overtime"};
    String[] keyTimeControls = {"s", "b", "c"};
    String[] stringPresets = {"Custom", "Bullet (1 min)", "Blitz (5 mins)",
            "Rapid (10 mins)", "Tournament (2 Hrs)"};
    String[] keyPresets = {"c", "b1", "b5", "r", "t"};

    Button buttonSave;

    Switch switchBlacksTurn;

    Spinner spinnerTimeControl;

    Switch switchTimeLowBeep;
    LinearLayout linearLayoutTimeLow;
    TextView textViewTimeLowVal;
    static int timeLowSec;

    Spinner spinnerPreset;

    LinearLayout linearLayoutMainTimeWhite;
    LinearLayout linearLayoutMainTimeBlack;
    TextView textViewMainTimeWhiteVal;
    TextView textViewMainTimeBlackVal;
    static int mainTimeAHH, mainTimeAMM, mainTimeASS;
    static int mainTimeBHH, mainTimeBMM, mainTimeBSS;

    Switch switchIncrement;
    LinearLayout linearLayoutIncrementWhite;
    LinearLayout linearLayoutIncrementBlack;
    TextView textViewIncrementWhiteVal;
    TextView textViewIncrementBlackVal;
    static int incrementValueA;
    static int incrementValueB;

    LinearLayout linearLayoutByoyomiWhite;
    LinearLayout linearLayoutByoyomiBlack;
    TextView textViewByoyomiTimeBlackVal;
    TextView textViewByoyomiTimeWhiteVal;
    static int byoyomiAMM, byoyomiASS;
    static int byoyomiBMM, byoyomiBSS;

    LinearLayout linearLayoutOvertimeWhite;
    LinearLayout linearLayoutOvertimeBlack;
    TextView textViewOvertimeWhiteVal;
    TextView textViewOvertimeBlackVal;
    static int overtimeAMM, overtimeASS;
    static int overtimeBMM, overtimeBSS;

    static boolean blacksTurn;

    static String timeControl;
    static boolean timeLow;
    static boolean increment;
    static String preset;
    static String mainTimeConcatenated;
    static String byoyomiTimeConcatenated;
    static String overtimeConcatenated;

    TextView exploreBQ;

    ArrayAdapter<String> arrayAdapterTimeControls;
    ArrayAdapter<String> arrayAdapterPresets;

    AlertDialog alertDialog;

    SharedPreferences preferences;

    NumberPicker numberPickerHH;
    NumberPicker numberPickerMM;
    NumberPicker numberPickerSS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor prefEditor = preferences.edit();

        buttonSave = findViewById(R.id.buttonSave);

        switchBlacksTurn = findViewById(R.id.switchBlacksTurn);

        spinnerTimeControl = findViewById(R.id.timeControlSpinner);

        switchTimeLowBeep = findViewById(R.id.switchTimeLowBeep);
        linearLayoutTimeLow = findViewById(R.id.linLayoutTimeLow);
        textViewTimeLowVal= findViewById(R.id.textViewTimeLowValue);

        spinnerPreset = findViewById(R.id.spinnerPreset);

        linearLayoutMainTimeWhite = findViewById(R.id.linearLayoutMainTimeWhite);
        linearLayoutMainTimeBlack = findViewById(R.id.linearLayoutMainTimeBlack);
        textViewMainTimeWhiteVal = findViewById(R.id.mainTimeWhiteValue);
        textViewMainTimeBlackVal = findViewById(R.id.mainTimeBlackValue);

        switchIncrement = findViewById(R.id.switchIncrement);
        linearLayoutIncrementWhite = findViewById(R.id.linearLayoutIncrementWhite);
        linearLayoutIncrementBlack = findViewById(R.id.linearLayoutIncrementBlack);
        textViewIncrementWhiteVal = findViewById(R.id.incrementWhiteValue);
        textViewIncrementBlackVal = findViewById(R.id.incrementBlackValue);

        linearLayoutByoyomiWhite = findViewById(R.id.linearLayoutByoyomiWhite);
        linearLayoutByoyomiBlack = findViewById(R.id.linearLayoutByoyomiBlack);
        textViewByoyomiTimeWhiteVal = findViewById(R.id.byoyomiTimeWhiteValue);
        textViewByoyomiTimeBlackVal = findViewById(R.id.byoyomiTimeBlackValue);

        linearLayoutOvertimeWhite = findViewById(R.id.linearLayoutOvertimeWhite);
        linearLayoutOvertimeBlack = findViewById(R.id.linearLayoutOvertimeBlack);
        textViewOvertimeWhiteVal = findViewById(R.id.overtimeWhiteValue);
        textViewOvertimeBlackVal = findViewById(R.id.overtimeBlackValue);
        exploreBQ = findViewById(R.id.textViewExploreBiotstoiq);

        blacksTurn = preferences.getBoolean("blacks_turn", false);
        switchBlacksTurn.setChecked(blacksTurn);

        arrayAdapterTimeControls = new ArrayAdapter<>(this, R.layout.spinner_list,
                R.id.spinnerItem, stringTimeControls);
        spinnerTimeControl.setAdapter(arrayAdapterTimeControls);
        timeControl = preferences.getString("time_control", "s");
        if(timeControl.equals("s")) {
            spinnerTimeControl.setSelection(0);
        } else if(timeControl.equals("b")) {
            spinnerTimeControl.setSelection(1);
        } else {
            spinnerTimeControl.setSelection(2);
        }

        timeLow = preferences.getBoolean("time_low", true);
        timeLowSec = preferences.getInt("time_low_sec", 10);
        switchTimeLowBeep.setChecked(timeLow);

        arrayAdapterPresets = new ArrayAdapter<>(this, R.layout.spinner_list,
                R.id.spinnerItem, stringPresets);
        spinnerPreset.setAdapter(arrayAdapterPresets);
        preset = preferences.getString("preset", "c");
        switch (preset) {
            case "c":
                spinnerPreset.setSelection(0);
                break;
            case "b1":
                spinnerPreset.setSelection(1);
                break;
            case "b5":
                spinnerPreset.setSelection(2);
                break;
            case "r":
                spinnerPreset.setSelection(3);
                break;
            default:
                spinnerPreset.setSelection(4);
                break;
        }

        linearLayoutTimeLow.setOnClickListener(v -> {
            numberPickerDialogBuild('s', "time_low");
            doTheSSThing();
            setNumberPickerSS(timeLowSec);
        });

        mainTimeAHH = preferences.getInt("main_hh_a", 0);
        mainTimeBHH = preferences.getInt("main_hh_b", 0);
        mainTimeAMM = preferences.getInt("main_mm_a", 10);
        mainTimeBMM = preferences.getInt("main_mm_b", 10);
        mainTimeASS = preferences.getInt("main_ss_a", 0);
        mainTimeBSS = preferences.getInt("main_ss_b", 0);

        increment = preferences.getBoolean("increment", false);
        switchIncrement.setChecked(increment);

        incrementValueA = preferences.getInt("increment_val_a", 5);
        incrementValueB = preferences.getInt("increment_val_b", 5);

        byoyomiAMM = preferences.getInt("byoyomi_mm_a", 1);
        byoyomiBMM = preferences.getInt("byoyomi_mm_b", 1);
        byoyomiASS = preferences.getInt("byoyomi_ss_a", 0);
        byoyomiBSS = preferences.getInt("byoyomi_ss_b", 0);

        overtimeAMM = preferences.getInt("overtime_mm_a", 1);
        overtimeBMM = preferences.getInt("overtime_mm_b", 1);
        overtimeASS = preferences.getInt("overtime_ss_a", 0);
        overtimeBSS = preferences.getInt("overtime_ss_b", 0);

        setTextViewValues();

        linearLayoutMainTimeWhite.setOnClickListener(v -> {
            numberPickerDialogBuild('h', "main_time_a");
            doTheHHThing();
            setNumberPickerHH(mainTimeAHH, mainTimeAMM, mainTimeASS);
        });

        linearLayoutMainTimeBlack.setOnClickListener(v -> {
            numberPickerDialogBuild('h', "main_time_b");
            doTheHHThing();
            setNumberPickerHH(mainTimeBHH, mainTimeBMM, mainTimeBSS);
        });

        linearLayoutIncrementWhite.setOnClickListener(v -> {
            numberPickerDialogBuild('s', "increment_val_a");
            doTheSSThing();
            setNumberPickerSS(incrementValueA);
        });

        linearLayoutIncrementBlack.setOnClickListener(v -> {
            numberPickerDialogBuild('s', "increment_val_b");
            doTheSSThing();
            setNumberPickerSS(incrementValueB);
        });

        linearLayoutByoyomiWhite.setOnClickListener(v -> {
            numberPickerDialogBuild('m', "byoyomi_time_a");
            doTheMMThing();
            setNumberPickerMM(byoyomiAMM, byoyomiASS);
        });

        linearLayoutByoyomiBlack.setOnClickListener(v -> {
            numberPickerDialogBuild('m', "byoyomi_time_b");
            doTheMMThing();
            setNumberPickerMM(byoyomiBMM, byoyomiBSS);
        });

        linearLayoutOvertimeWhite.setOnClickListener(v -> {
            numberPickerDialogBuild('m', "overtime_a");
            doTheMMThing();
            setNumberPickerMM(overtimeAMM, overtimeASS);
        });

        linearLayoutOvertimeBlack.setOnClickListener(v -> {
            numberPickerDialogBuild('m', "overtime_b");
            doTheMMThing();
            setNumberPickerMM(overtimeBMM, overtimeBSS);
        });

        buttonSave.setOnClickListener(v -> {
            prefEditor.putBoolean("blacks_turn", switchBlacksTurn.isChecked());
            prefEditor.putBoolean("time_low", switchTimeLowBeep.isChecked());
            prefEditor.putInt("time_low_sec", timeLowSec);
            prefEditor.putBoolean("increment", switchIncrement.isChecked());
            prefEditor.putString("time_control", keyTimeControls[spinnerTimeControl.getSelectedItemPosition()]);
            prefEditor.putString("preset", keyPresets[spinnerPreset.getSelectedItemPosition()]);

            prefEditor.putInt("main_hh_a", mainTimeAHH);
            prefEditor.putInt("main_hh_b", mainTimeBHH);
            prefEditor.putInt("main_mm_a", mainTimeAMM);
            prefEditor.putInt("main_mm_b", mainTimeBMM);
            prefEditor.putInt("main_ss_a", mainTimeASS);
            prefEditor.putInt("main_ss_b", mainTimeBSS);

            prefEditor.putInt("byoyomi_mm_a", byoyomiAMM);
            prefEditor.putInt("byoyomi_mm_b", byoyomiBMM);
            prefEditor.putInt("byoyomi_ss_a", byoyomiASS);
            prefEditor.putInt("byoyomi_ss_b", byoyomiBSS);

            prefEditor.putInt("overtime_mm_a", overtimeAMM);
            prefEditor.putInt("overtime_mm_b", overtimeBMM);
            prefEditor.putInt("overtime_ss_a", overtimeASS);
            prefEditor.putInt("overtime_ss_b", overtimeBSS);
            prefEditor.apply();
        });

        exploreBQ.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private void numberPickerDialogBuild(char type, String key) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(R.string.choose_value);
        alertDialogBuilder.setNegativeButton(R.string.cancel, ((dialog, which) -> dialog.cancel()));
        switch (type) {
            case 'h':
                alertDialogBuilder.setView(R.layout.numberpicker_hh_mm_ss);
                alertDialogBuilder.setPositiveButton(R.string.ok, ((dialog, which) -> {
                    hh = numberPickerHH.getValue();
                    mm = numberPickerMM.getValue();
                    ss = numberPickerSS.getValue();
                    setMainTime(key.charAt(key.length()-1), hh, mm, ss);
                    setTextViewValues();
                }));
                break;
            case 'm':
                alertDialogBuilder.setView(R.layout.numberpicker_mm_ss);
                alertDialogBuilder.setPositiveButton(R.string.ok, ((dialog, which) -> {
                    mm = numberPickerMM.getValue();
                    ss = numberPickerSS.getValue();
                    if(key.equals("byoyomi_time_a") || key.equals("byoyomi_time_b")) {
                        setByoyomiTime(key.charAt(key.length()-1), mm, ss);
                    } else if(key.equals("overtime_a") || key.equals("overtime_b")) {
                        setOvertime(key.charAt(key.length()-1), mm, ss);
                    }
                    setTextViewValues();
                }));
                break;
            case 's':
                alertDialogBuilder.setView(R.layout.numberpicker_ss);
                alertDialogBuilder.setPositiveButton(R.string.ok, ((dialog, which) -> {
                    ss = numberPickerSS.getValue();
                    if (key.equals("time_low")) {
                        setTimeLow(ss);
                    } else if(key.equals("increment_val_a") || key.equals("increment_val_b")) {
                        setIncrementValue(key.charAt(key.length()-1), ss);
                    }
                    setTextViewValues();
                }));
                setTextViewValues();
                break;
            default:
                break;
        }
        alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void setTextViewValues() {

        textViewTimeLowVal.setText(String.valueOf(timeLowSec));

        mainTimeConcatenated = String.valueOf(mainTimeAHH)
                .concat(":")
                .concat(String.valueOf(mainTimeAMM))
                .concat(":")
                .concat(String.valueOf(mainTimeASS));

        textViewMainTimeWhiteVal.setText(mainTimeConcatenated);

        mainTimeConcatenated = String.valueOf(mainTimeBHH)
                .concat(":")
                .concat(String.valueOf(mainTimeBMM))
                .concat(":")
                .concat(String.valueOf(mainTimeBSS));

        textViewMainTimeBlackVal.setText(mainTimeConcatenated);

        textViewIncrementWhiteVal.setText(String.valueOf(incrementValueA));
        textViewIncrementBlackVal.setText(String.valueOf(incrementValueB));

        byoyomiTimeConcatenated = String.valueOf(byoyomiAMM)
                .concat(":")
                .concat(String.valueOf(byoyomiASS));

        textViewByoyomiTimeWhiteVal.setText(byoyomiTimeConcatenated);

        byoyomiTimeConcatenated = String.valueOf(byoyomiBMM)
                .concat(":")
                .concat(String.valueOf(byoyomiBSS));

        textViewByoyomiTimeBlackVal.setText(byoyomiTimeConcatenated);

        overtimeConcatenated = String.valueOf(overtimeAMM)
                .concat(":")
                .concat(String.valueOf(overtimeASS));

        textViewOvertimeWhiteVal.setText(overtimeConcatenated);

        overtimeConcatenated = String.valueOf(overtimeBMM)
                .concat(":")
                .concat(String.valueOf(overtimeBSS));

        textViewOvertimeBlackVal.setText(overtimeConcatenated);
    }

    private void setMinMax(char t, NumberPicker numberPicker) {
        switch (t) {
            case 'h':
                numberPicker.setMaxValue(72);
                break;
            case 'm':
            case 's':
                numberPicker.setMaxValue(60);
                break;
        }
        numberPicker.setMinValue(0);
    }

    private void setTimeLow(int ss) {
        timeLowSec = ss;
    }

    private void setMainTime(char aOrB, int hh, int mm, int ss) {
        switch (aOrB) {
            case 'a':
                mainTimeAHH = hh;
                mainTimeAMM = mm;
                mainTimeASS = ss;
                break;
            case 'b':
                mainTimeBHH = hh;
                mainTimeBMM = mm;
                mainTimeBSS = ss;
        }
    }

    private void setIncrementValue(char aOrB, int ss) {
        switch (aOrB) {
            case 'a':
                incrementValueA = ss;
                break;
            case 'b':
                incrementValueB = ss;
                break;
        }
    }

    private void setByoyomiTime(char aOrB, int mm, int ss) {
        switch (aOrB) {
            case 'a':
                byoyomiAMM = mm;
                byoyomiASS = ss;
                break;
            case 'b':
                byoyomiBMM = mm;
                byoyomiBSS = ss;
        }
    }

    private void setOvertime(char aOrB, int mm, int ss) {
        switch (aOrB) {
            case 'a':
                overtimeAMM = mm;
                overtimeASS = ss;
                break;
            case 'b':
                overtimeBMM = mm;
                overtimeBSS = ss;
        }
    }

    private void setNumberPickerHH(int hh, int mm, int ss) {
        numberPickerHH.setValue(hh);
        setNumberPickerMM(mm, ss);
    }
    private void setNumberPickerMM(int mm, int ss) {
        numberPickerMM.setValue(mm);
        setNumberPickerSS(ss);
    }
    private void setNumberPickerSS(int ss) {
        numberPickerSS.setValue(ss);
    }

    private void doTheHHThing() {
        numberPickerHH = alertDialog.findViewById(R.id.numberPickerHH);
        setMinMax('h', numberPickerHH);
        doTheMMThing();
        doTheSSThing();
    }

    private void doTheMMThing() {
        numberPickerMM = alertDialog.findViewById(R.id.numberPickerMM);
        setMinMax('m', numberPickerMM);
        doTheSSThing();
    }


    private void doTheSSThing() {
        numberPickerSS = alertDialog.findViewById(R.id.numberPickerSS);
        setMinMax('s', numberPickerSS);
    }

}
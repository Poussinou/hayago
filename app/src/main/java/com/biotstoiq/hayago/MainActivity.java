package com.biotstoiq.hayago;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.Button;

import java.util.Locale;

public class MainActivity extends Activity {

    static boolean isPaused = true;
    static boolean pausedOnSettingsPause = false;
    static boolean fromBeginning = true;
    static boolean blacksTurn;
    static boolean someoneLost;

    static boolean byoyomiMode;
    static boolean overtimeMode;
    static boolean byoyomiEnteredA, byoyomiEnteredB;

    static boolean increment;
    static int incrementTimeA, incrementTimeB;

    static int hoursA, hoursB;
    static int mainTimeMinsA, mainTimeSecsA;
    static int mainTimeMinsB, mainTimeSecsB;
    static int remTimeA, remTimeB;

    static boolean timeLowBeepOn;
    static int timeLowBeepSec;
    static int totTime = 0;

    static int byoyomiTimePeriodMinsA, byoyomiTimePeriodSecsA;
    static int byoyomiTimePeriodMinsB, byoyomiTimePeriodSecsB;
    static int byoyomiTotTimeA, byoyomiTotTimeB;
    static int byoyomiPeriodA, byoyomiPeriodB;

    static int overtimeMovesAC, overtimeMovesBC;
    static int overtimeMovesA, overtimeMovesB;
    static int overtimeMinsA, overtimeSecsA;
    static int overtimeMinsB, overtimeSecsB;
    static int overtimeTotA, overtimeTotB;

    static String gameMode;
    static String preset;

    Locale l = Locale.UK;

    CountDownTimer timer;

    Button playPauseButton;
    Button resetButton;
    Button settingsButton;
    Button helpButton;
    Button buttonA;
    Button buttonAA;
    Button buttonAB;
    Button buttonB;
    Button buttonBA;
    Button buttonBB;
    Button currButton;

    AlertDialog.Builder resetDialog;
    AlertDialog.Builder helpDialog;

    static MediaPlayer stasto;
    static MediaPlayer p1Audio;
    static MediaPlayer p2Audio;
    static MediaPlayer low_tick;
    static MediaPlayer timeUpAudio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        playPauseButton = findViewById(R.id.buttonPausePlay);
        buttonA = findViewById(R.id.buttonA);
        buttonAA = findViewById(R.id.buttonAA);
        buttonAB = findViewById(R.id.buttonAB);
        buttonB = findViewById(R.id.buttonB);
        buttonBA = findViewById(R.id.buttonBA);
        buttonBB = findViewById(R.id.buttonBB);
        resetButton = findViewById(R.id.buttonReset);
        settingsButton = findViewById(R.id.buttonSettings);
        helpButton = findViewById(R.id.buttonHelp);

        initializeValues();
        initializeButtons();

        stasto = MediaPlayer.create(this, R.raw.stasto);
        p1Audio = MediaPlayer.create(this, R.raw.switch1);
        p2Audio = MediaPlayer.create(this, R.raw.switch2);
        low_tick = MediaPlayer.create(this, R.raw.tick);
        timeUpAudio = MediaPlayer.create(this, R.raw.time_ended);

        playPauseButton.setOnClickListener(v -> wholeTimerFunc());
        resetButton.setOnClickListener(v -> {
            resetDialog = new AlertDialog.Builder(this);
            resetDialog.setTitle("Reset");
            resetDialog.setMessage("Are you sure?");
            resetDialog.setNegativeButton("No", (dialog, which) -> {
            });
            resetDialog.setPositiveButton("Yes", (dialog, which) -> resetStuff());
            resetDialog.create().show();
        });
        settingsButton.setOnClickListener(v -> {
            pausedOnSettingsPause = true;
            Intent settingsIntent = new Intent(MainActivity.this, SettingsActivity.class);
            startActivity(settingsIntent);
        });
        helpButton.setOnClickListener(v -> {
            if(!isPaused) {
                isPaused = true;
                timer.cancel();
                timer = null;
            }
            helpDialog = new AlertDialog.Builder(this);
            helpDialog.setTitle("Help");
            helpDialog.setMessage(R.string.help_text);
            helpDialog.setPositiveButton("OK", (dialog, which) -> {
            });
            helpDialog.setNeutralButton(R.string.rate, ((dialog, which) -> {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(
                        "https://play.google.com/store/apps/details?id=com.biotstoiq.hayago"));
                intent.setPackage("com.android.vending");
                startActivity(intent);
            }));
            helpDialog.create().show();
        });
        buttonA.setOnClickListener(v -> {
            if(!blacksTurn) {
                if(!isPaused) {
                    timer.cancel();
                    timer = null;
                    p1Audio.start();
                    if(byoyomiMode) {
                        if(byoyomiEnteredA) {
                            totTime = remTimeA = byoyomiTotTimeA - 1;
                        }
                    }
                    if(overtimeMode) {
                        if(byoyomiEnteredA) {
                            overtimeMovesA--;
                            if(overtimeMovesA == 0) {
                                overtimeMovesA = overtimeMovesAC;
                                totTime = remTimeA = overtimeTotA - 1;
                            }
                            buttonAB.setText(String.valueOf(overtimeMovesA));
                        }
                    }
                    blacksTurn = true;
                    buttonB.setTextColor(0xffffffff);
                    buttonA.setTextColor(0xff808080);
                    totTime = remTimeB+1;
                    if(increment) {
                        remTimeA = remTimeA+incrementTimeA;
                    }
                    buttonA.setText(String.format(l, "%02d",
                            remTimeA/60).concat(":".concat(String.format(l, "%02d", remTimeA%60))));
                    currButton = buttonB;
                    timerFunc();
                }
            }
        });
        buttonB.setOnClickListener(v -> {
            if(blacksTurn) {
                if(!isPaused) {
                    timer.cancel();
                    timer = null;
                    p2Audio.start();
                    if(byoyomiMode) {
                        if(byoyomiEnteredB) {
                            totTime = remTimeB = byoyomiTotTimeB - 1;
                        }
                    }
                    if(overtimeMode) {
                        if(byoyomiEnteredB) {
                            overtimeMovesB--;
                            if(overtimeMovesB == 0) {
                                overtimeMovesB = overtimeMovesBC;
                                totTime = remTimeB = overtimeTotB - 1;
                            }
                            buttonBA.setText(String.valueOf(overtimeMovesB));
                        }
                    }
                    blacksTurn = false;
                    buttonA.setTextColor(0xff000000);
                    buttonB.setTextColor(0xff888888);
                    totTime = remTimeA+1;
                    if(increment) {
                        remTimeB = remTimeB+incrementTimeB;
                    }
                    buttonB.setText(String.format(l, "%02d",
                            remTimeB/60).concat(
                            ":".concat(String.format(l, "%02d", remTimeB%60))));
                    currButton = buttonA;
                    timerFunc();
                }
            }
        });
    }
    public void wholeTimerFunc() {
        stasto.start();
        if(someoneLost) {
            resetStuff();
            someoneLost = false;
        }
        if(!isPaused) {
            isPaused = true;
            timer.cancel();
            timer = null;
        } else {
            isPaused = false;
            if(fromBeginning) {
                fromBeginning = false;
                if(blacksTurn) {
                    buttonB.setTextColor(0xffffffff);
                    totTime = remTimeB;
                    remTimeA--;
                } else {
                    buttonA.setTextColor(0xff000000);
                    totTime = remTimeA;
                    remTimeB--;
                }
            }
            if(blacksTurn) {
                currButton = buttonB;
            } else {
                currButton = buttonA;
            }
            timerFunc();
        }
    }
    public void timerFunc() {
        timer = new CountDownTimer(
                totTime* 1000L, 1000) {
            @Override
            public void onTick() {
                ticky();
            }
        };
        timer.start();
    }

    public void ticky() {
        if(!blacksTurn) {
            remTimeA--;
        } else {
            remTimeB--;
        }
        totTime--;
        currButton.setText(String.format(l, "%02d",
                totTime/60).concat(
                ":".concat(String.format(l, "%02d", totTime%60))));
        if((totTime <= timeLowBeepSec) && timeLowBeepOn) {
            low_tick.start();
        }
        if(totTime <= 0) {
            timer.cancel();
            timer = null;
            if(!byoyomiMode && !overtimeMode) {
                timerFinished();
            } else {
                if(byoyomiMode) {
                    if(byoyomiEnteredA || byoyomiEnteredB) {
                        if(!blacksTurn) {
                            if(byoyomiEnteredA) {
                                if(byoyomiPeriodA > 1) {
                                    byoyomiPeriodA--;
                                    totTime = remTimeA = byoyomiTotTimeA;
                                    buttonAB.setText(String.valueOf(byoyomiPeriodA));
                                    timerFunc();
                                } else {
                                    timerFinished();
                                }
                                return;
                            }
                        } else {
                            if(byoyomiEnteredB) {
                                if(byoyomiPeriodB > 1) {
                                    byoyomiPeriodB--;
                                    totTime = remTimeB = byoyomiTotTimeB;
                                    buttonBA.setText(String.valueOf(byoyomiPeriodB));
                                    timerFunc();
                                } else {
                                    timerFinished();
                                }
                                return;
                            }
                        }
                    }
                    if(!blacksTurn) {
                        byoyomiEnteredA = true;
                        totTime = remTimeA = byoyomiTotTimeA;
                    } else {
                        byoyomiEnteredB = true;
                        totTime = remTimeB = byoyomiTotTimeB;
                    }
                } else {
                    if(byoyomiEnteredA || byoyomiEnteredB) {
                        if ((blacksTurn && byoyomiEnteredB) || (!blacksTurn && byoyomiEnteredA)) {
                            timerFinished();
                            return;
                        }
                    }
                    if(!blacksTurn) {
                        byoyomiEnteredA = true;
                        totTime = remTimeA = overtimeTotA;
                    } else {
                        byoyomiEnteredB = true;
                        totTime = remTimeB = overtimeTotB;
                    }
                }
                timerFunc();
            }
        }
    }
    public void timerFinished() {
        isPaused = true;
        timeUpAudio.start();
        if(blacksTurn) {
            buttonB.setText(R.string.zeros_button);
        } else {
            buttonA.setText(R.string.zeros_button);
        }
        someoneLost = true;
    }
    public void resetStuff() {
        fromBeginning = true;
        initializeValues();
        initializeButtons();
        if(!isPaused) {
            timer.cancel();
            isPaused = true;
        }
    }
    public void initializeValues() {
        SharedPreferences preference = PreferenceManager.getDefaultSharedPreferences(this);
        hoursA = preference.getInt("main_hh_a", 0);
        hoursB = preference.getInt("main_hh_b", 0);
        mainTimeMinsA = preference.getInt("main_mm_a", 10);
        mainTimeSecsA = preference.getInt("main_ss_a", 0);
        mainTimeMinsB = preference.getInt("main_mm_b", 10);
        mainTimeSecsB = preference.getInt("main_ss_b", 0);

        byoyomiPeriodA = preference.getInt("byoyomi_periods_a", 5);
        byoyomiPeriodB = preference.getInt("byoyomi_periods_b", 5);
        byoyomiTimePeriodMinsA = preference.getInt("byoyomi_mm_a", 1);
        byoyomiTimePeriodSecsA = preference.getInt("byoyomi_ss_a", 0);
        byoyomiTimePeriodMinsB = preference.getInt("byoyomi_mm_b", 1);
        byoyomiTimePeriodSecsB = preference.getInt("byoyomi_ss_b", 0);
        byoyomiTotTimeA = (byoyomiTimePeriodMinsA*60) + byoyomiTimePeriodSecsA + 1;
        byoyomiTotTimeB = (byoyomiTimePeriodMinsB*60) + byoyomiTimePeriodSecsB + 1;

        overtimeMinsA = preference.getInt("overtime_mm_a", 1);
        overtimeSecsA = preference.getInt("overtime_ss_a", 0);
        overtimeMinsB = preference.getInt("overtime_mm_b", 1);
        overtimeSecsB = preference.getInt("overtime_ss_b", 0);
        overtimeMovesA = preference.getInt("overtime_moves_a", 5);
        overtimeMovesB = preference.getInt("overtime_moves_b", 5);
        overtimeMovesAC = overtimeMovesA;
        overtimeMovesBC = overtimeMovesB;
        overtimeTotA = (overtimeMinsA*60) + overtimeSecsA + 1;
        overtimeTotB = (overtimeMinsB*60) + overtimeSecsB + 1;

        increment = preference.getBoolean("increment", false);
        if(increment) {
            incrementTimeA = preference.getInt("increment_val_a", 5);
            incrementTimeB = preference.getInt("increment_val_b", 5);
        }
        blacksTurn = preference.getBoolean("blacks_turn", false);
        gameMode = preference.getString("time_control", "s");
        preset = preference.getString("preset", "c");

        timeLowBeepOn = preference.getBoolean("time_low", true);
        timeLowBeepSec = preference.getInt("time_low_sec", 10);
        byoyomiEnteredA = byoyomiEnteredB = false;

        someoneLost = false;
    }
    public void initializeButtons() {
        switch (preset) {
            case "b1":
                mainTimeMinsA = mainTimeMinsB = 1;
                mainTimeSecsA = mainTimeSecsB = 0;
                break;
            case "b5":
                mainTimeMinsA = mainTimeMinsB = 5;
                mainTimeSecsA = mainTimeSecsB = 0;
                break;
            case "r":
                mainTimeMinsA = mainTimeMinsB = 10;
                mainTimeSecsA = mainTimeSecsB = 0;
                break;
            case "t":
                mainTimeMinsA = mainTimeMinsB = 120;
                mainTimeSecsA = mainTimeSecsB = 0;
                break;
            default:
                break;
        }

        remTimeA = (hoursA*3600) + mainTimeSecsA + (mainTimeMinsA*60) + 1;
        remTimeB = (hoursB*3600) + mainTimeSecsB + (mainTimeMinsB*60) + 1;

        String mainTimeStringA =
                String.format(l, "%02d", remTimeA/60) + ":" +
                        String.format(l, "%02d", mainTimeSecsA);
        String mainTimeStringB =
                String.format(l, "%02d", remTimeB/60) + ":" +
                        String.format(l, "%02d", mainTimeSecsB);
        buttonA.setText(mainTimeStringA);
        buttonA.setBackgroundColor(0xffffffff);
        buttonA.setTextColor(0xff888888);
        buttonB.setText(mainTimeStringB);
        buttonB.setBackgroundColor(0xff000000);
        buttonB.setTextColor(0xff888888);

        switch (gameMode) {
            case "s":
                byoyomiMode = false;
                overtimeMode = false;
                buttonAA.setText(R.string.off);
                buttonAB.setText(R.string.off);
                buttonBA.setText(R.string.off);
                buttonBB.setText(R.string.off);
                break;
            case "b":
                byoyomiMode = true;
                overtimeMode = false;
                commonButtonsA(byoyomiPeriodA, byoyomiTimePeriodMinsA, byoyomiTimePeriodSecsA);
                commonButtonsB(byoyomiPeriodB, byoyomiTimePeriodMinsB, byoyomiTimePeriodSecsB);
                break;
            case "c":
                overtimeMode = true;
                byoyomiMode = false;
                commonButtonsA(overtimeMovesA, overtimeMinsA, overtimeSecsA);
                commonButtonsB(overtimeMovesB, overtimeMinsB, overtimeSecsB);
            default:
                break;
        }
    }

    public void commonButtonsA(int C1, int C2, int C3) {
        String byovertime = String.format(l, "%02d", C2).concat(":".concat(
                String.format(l, "%02d", C3)));
        buttonAB.setText(String.valueOf(C1));
        buttonAA.setText(byovertime);
    }

    public void commonButtonsB(int C1, int C2, int C3) {
        String byovertime = String.format(l, "%02d", C2).concat(":".concat(
                String.format(l, "%02d", C3)));
        buttonBA.setText(String.valueOf(C1));
        buttonBB.setText(byovertime);
    }
    public void onPause() {
        super.onPause();
        if(!isPaused) {
            isPaused = true;
            timer.cancel();
            timer = null;
        }
    }
    public void onResume() {
        super.onResume();
        if(pausedOnSettingsPause) {
            pausedOnSettingsPause = false;
            resetStuff();
        }
    }
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setTitle("Exit")
                .setMessage("Are you sure?")
                .setPositiveButton("Yes", (dialog, which) -> finish())
                .setNegativeButton("No", null)
                .show();
    }
}
